#!/usr/bin/env bash
yarn run webpack:build
./mvnw -Pdev,no-liquibase -DskipTests=true package
java -Xmx6G -Dschedule=true -DprocessEmails=true -jar target/exp-0.0.1-SNAPSHOT.war
