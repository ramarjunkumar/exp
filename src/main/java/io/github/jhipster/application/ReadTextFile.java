package io.github.jhipster.application;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.nio.file.Files;

import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;

import io.github.jhipster.application.domain.FieldsBean;
import io.github.jhipster.application.domain.ImagesBean;
import io.github.jhipster.application.web.rest.FieldsResource;


public class ReadTextFile {

	 @SuppressWarnings("resource")
     public void getFile(String name1,String name2,String unzipname, List<String> finalpdflist,List<String> finaljpglist) throws IOException {

         List<String> results = new ArrayList<String>();


         List<FieldsBean> fieldsBean = new ArrayList<FieldsBean>();
         List<ImagesBean> imagesBeanList = new ArrayList<ImagesBean>();

         File folder = new File("src/main/resources/config1/"+unzipname);


       //  File[] files = ResourceUtils.getFile("classpath:config1/"+unzipname).listFiles();

         File[] files =  folder.listFiles();


//If this pathname does not denote a directory, then listFiles() returns null.
         Boolean val = false;
         for (File file : files) {
             if (file.isFile()) {
                // results.add(file.getName());



                if(file.getName().equals("fields.dat")){
                    val = true;
                };
             }

         }

         System.out.println("Boolean value----->"+val);

         if(val){

            // File file1 = ResourceUtils.getFile("classpath:config1/"+unzipname+"/"+name1);

             File file1 = new File("src/main/resources/config1/"+unzipname+"/"+name1);

             LineNumberReader lineNumberReader = new LineNumberReader(new FileReader(file1));
             lineNumberReader.skip(Long.MAX_VALUE);
             int line = lineNumberReader.getLineNumber();
             //System.out.println("Lines" + line);
             lineNumberReader.close();


             String content = new String(Files.readAllBytes(file1.toPath()));

             BufferedReader b = null;

             b = new BufferedReader(new FileReader(file1));




             if (name1.equals("fields.dat")) {

                 int counter = 1;
                 String rowNum = "";
                 String rowCheck = "";


                 FieldsBean addFieldsBean = new FieldsBean();

                 while ((content = b.readLine()) != null) {
                     String regex = "(\\s)+";
                     String[] row = content.trim().split(regex);


                     rowNum = row[3];
                     if (rowCheck.equals("")) {
                         rowCheck = rowNum;
                     }
                     if (rowNum.equals(rowCheck)) {
                         String valueRow = row[6] + " " + row[7];

                         if (valueRow.equals("CHECK DATE"))
                             addFieldsBean.setCheckDate(row[8]);
                         if (valueRow.equals("INVOICE NUMBER") || valueRow.equals("TRANS ID"))
                             addFieldsBean.setInvoiceNo(row[8]);
                         if (valueRow.equals("INVOICE AMOUNT"))
                             addFieldsBean.setInvoiceamt(row[8]);

                     } else if (!rowNum.equals(rowCheck)) {

                         fieldsBean.add(addFieldsBean);
                         addFieldsBean = new FieldsBean();
                         String valueRow = row[6] + " " + row[7];
                         rowCheck = rowNum;

                         if (valueRow.equals("CHECK DATE"))
                             addFieldsBean.setCheckDate(row[8]);
                         if (valueRow.equals("INVOICE NUMBER") || valueRow.equals("TRANS ID"))
                             addFieldsBean.setInvoiceNo(row[8]);
                         if (valueRow.equals("INVOICE AMOUNT"))
                             addFieldsBean.setInvoiceamt(row[8]);

                     }
                    // System.out.println(counter);
                     if (counter == line) {
                         fieldsBean.add(addFieldsBean);
                     }
                     counter++;
                 }


             }

}

         File file2 = new File("src/main/resources/config1/"+unzipname+"/"+name2);
           // File file2 = ResourceUtils.getFile("classpath:config1/"+unzipname+"/"+name2);
             LineNumberReader lineNumberReader1 = new LineNumberReader(new FileReader(file2));
             lineNumberReader1.skip(Long.MAX_VALUE);
             int line1 = lineNumberReader1.getLineNumber();
            // System.out.println("Lines" + line1);
             lineNumberReader1.close();


             //File is found
             //System.out.println("File Found : " + file2.exists());

             //Read File Content
             String content1 = new String(Files.readAllBytes(file2.toPath()));
             //System.out.println(content);
             BufferedReader b1 = null;

             b1 = new BufferedReader(new FileReader(file2));

           List<String> test = new ArrayList<String>();

             if (name2.equals("images.dat")) {


                 while ((content1 = b1.readLine()) != null) {


                     String regex = "(\\s)+";
                     String[] row = content1.trim().split(regex);

                    // System.out.println(Arrays.toString(row));


                     if (row[4].equals("1")) {
                         double f = Double.parseDouble(row[6]);
                         // System.out.println(String.format("%.2f", new BigDecimal(f)));

                         ImagesBean imagesbean = new ImagesBean();
                         imagesbean.setAmount(String.format("%.2f", new BigDecimal(f)));
                         imagesbean.setRouting_number(row[7]);
                         imagesbean.setAccount_number(row[8]);
                         test.add(row[9]);
                         imagesBeanList.add(imagesbean);

                     }


                 }


             }


         System.out.println("testlist.......tif................."+test);

         List<String> subtest = new ArrayList<String>();

         for(int i=0;i<test.size();i++) {
             String result = test.get(i).substring(0, test.get(i).lastIndexOf("."));
             subtest.add(result);
             System.out.println("subtest list............."+subtest);

         }
         System.out.println("Field--->"+fieldsBean+fieldsBean.size() );
         System.out.println("Images---->"+imagesBeanList+imagesBeanList.size());
         System.setProperty("hadoop.home.dir", "E:/Win");

         List<String> pdlist = new ArrayList<String>();
         List<String> jplist = new ArrayList<String>();


         for(int j=0;j<subtest.size();j++) {
             for(int k=0;k<finalpdflist.size();k++) {
                 if(finalpdflist.get(k).contains(subtest.get(j))) {
                     pdlist.add(finalpdflist.get(k));
                 }
             }
         }


         for(int j=0;j<subtest.size();j++) {
             for(int k=0;k<finaljpglist.size();k++) {
                 if(finaljpglist.get(k).contains(subtest.get(j))) {
                     jplist.add(finaljpglist.get(k));
                 }
             }
         }

         System.out.println("ssssssssssssssssssssssssssssssss"+pdlist);
         System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy"+jplist);



				 
				 System.out.println("Field--->"+fieldsBean+fieldsBean.size() );
				 System.out.println("Images---->"+imagesBeanList+imagesBeanList.size());
				 System.setProperty("hadoop.home.dir", "E:/Win");

				 FieldsResource.insertData(fieldsBean,imagesBeanList,pdlist,jplist);
				 
				 
				 
				 

	
}
	 
	
	
}
