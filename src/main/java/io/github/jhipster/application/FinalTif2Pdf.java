package io.github.jhipster.application;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import com.itextpdf.text.pdf.codec.TiffImage;
import io.github.jhipster.application.service.UploadToS3;

public class FinalTif2Pdf {

    String pdf_hd="";

    public List<String> fileLocation=new ArrayList<String>();

    public List<String> getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(List<String> fileLocation) {
        this.fileLocation = fileLocation;
    }

    public void tiffPdf(String sFilesDir) {
        File[] filesInDirectory = new File(sFilesDir).listFiles();
        String finalPath =sFilesDir;
        System.out.println("finalPath first"+finalPath);
        for(File f : filesInDirectory){
            if(f.isDirectory()){
                tiffPdf(f.getAbsolutePath());
               // System.out.println("Absolute Path "+f.getAbsolutePath());
                finalPath = f.getAbsolutePath();
                //System.out.println("Final Path"+finalPath);
            }
            else {
                process(finalPath);
                break;
            }
        }


    }

    public void process(String sFilesDir) {
       // System.out.println(sFilesDir);
        File[] filesInDirectory = new File(sFilesDir).listFiles();

        for(File f : filesInDirectory){
            if(f.exists()) {
                String filePath = f.getAbsolutePath();
                //System.out.println("filepath pdf............................"+filePath);
                String fileExtension = filePath.substring(filePath.lastIndexOf(".") + 1,filePath.length());
               // System.out.println("fileExtension...................."+fileExtension);
                //if("TIF".equals(fileExtension)){
                try{
                    if("TIF".equals(fileExtension)) {
                        RandomAccessFileOrArray myTiffFile=new RandomAccessFileOrArray(filePath);
                        int numberOfPages=TiffImage.getNumberOfPages(myTiffFile);
                        String newfiles=filePath.replace(".TIF",".pdf");
                        Document TifftoPDF=new Document();
                        PdfWriter.getInstance(TifftoPDF, new FileOutputStream(newfiles));
                        TifftoPDF.open();
                        for(int i=1;i<=numberOfPages;i++){
                            Image tempImage=TiffImage.getTiffImage(myTiffFile, i);
                            Rectangle pageSize = new Rectangle(tempImage.getWidth(),tempImage.getHeight());
                            TifftoPDF.setPageSize(pageSize);
                            TifftoPDF.newPage();
                            TifftoPDF.add(tempImage);
                        }
                        TifftoPDF.close();
                      //  System.out.println("PATH>>>>>>>>>>>>>>>>>>>>>>>"+sFilesDir);

                        UploadToS3 awsConnect = new UploadToS3();
                        String bucketName = "";
                        String keyName = "";
                        String str=newfiles;
                      //  System.out.println("str............................"+str);
                        String substr = "config";
					    String[] parts = str.split(substr);
                       // System.out.println("parts..........................."+parts);
                        String after = parts[1];
                     //   System.out.println("after..........................."+after);
                        String Finalsplitpath =after.substring(0, after.length());
                      //  System.out.println("........>>>>>>>>pdf........."+Finalsplitpath);
                        Finalsplitpath=Finalsplitpath.replace("/", "");
                       // System.out.println("FINAL>>>>>>>>pdf........."+Finalsplitpath);
					    AmazonS3 s3Client = new AmazonS3Client(DefaultAWSCredentialsProviderChain.getInstance());
                        UploadToS3 s3 = new UploadToS3();
					    pdf_hd=s3.S3Upload(s3Client, "exp-sandbox-wellsfargo-output/",Finalsplitpath,
                            str);
                        fileLocation.add(pdf_hd);
                      //  System.out.println(fileLocation);
                        //setFileLocation(fileLocation);
                      //  System.out.println(fileLocation);
                    }else if("JPG".equalsIgnoreCase(fileExtension)) {
                        f.delete();
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        }


    }


}
