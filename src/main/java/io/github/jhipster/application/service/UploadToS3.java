package io.github.jhipster.application.service;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UploadToS3 {




    public AmazonS3 s3ClientConnect(String accessKey, String secretKey) {
        AWSCredentials credentials = new BasicAWSCredentials(accessKey,
            secretKey);
        AmazonS3 s3client = new AmazonS3Client(credentials);
        // s3client.setObjectAcl(existingBucketName, keyName,
        // CannedAccessControlList.Private);
        return s3client;
    }

    public String S3Upload(AmazonS3 s3Client, String existingBucketName,
                           String keyName, String filePath) throws IOException {
        String eTag = "";
        List<PartETag> partETags = new ArrayList<PartETag>();
        InitiateMultipartUploadRequest initRequest = new InitiateMultipartUploadRequest(
            existingBucketName, keyName).withCannedACL(CannedAccessControlList.PublicRead);
        initRequest.putCustomRequestHeader("x-amz-server-side-encryption", "aws:kms");
        //initRequest.putCustomRequestHeader("x-amz-server-side-encryption-aws-kms-key-id", "98006c10-1598-4c41-9d60-9b86b36c4339");
        InitiateMultipartUploadResult initResponse = s3Client
            .initiateMultipartUpload(initRequest);
        System.out.println("FILEPATH................"+filePath);
        File file = new File(filePath);
        System.out.println("FILE... UPLOAD S3............."+file);
        long contentLength = file.length();
        System.out.println("Contentlength-------------------------"+contentLength);
        long partSize = 5242880;

        System.out.println("testi>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        try {
            System.out.println("Entered..................................................");
            long filePosition = 0;
            for (int i = 1; filePosition < contentLength; i++) {
                partSize = Math.min(partSize, (contentLength - filePosition));
                UploadPartRequest uploadRequest = new UploadPartRequest()
                    .withBucketName(existingBucketName).withKey(keyName)
                    .withUploadId(initResponse.getUploadId())
                    .withPartNumber(i).withFileOffset(filePosition)
                    .withFile(file).withPartSize(partSize);
                partETags.add(s3Client.uploadPart(uploadRequest).getPartETag());
                filePosition += partSize;
            }
            CompleteMultipartUploadRequest compRequest = new CompleteMultipartUploadRequest(
                existingBucketName, keyName, initResponse.getUploadId(),
                partETags);
            CompleteMultipartUploadResult result = s3Client
                .completeMultipartUpload(compRequest);
            eTag = result.getETag();

            System.out.println("tes???????????????????????????????????????????????????????????????????????????????????????????");
            if (eTag != null) {
                System.out.println("uploaded sucessfully");
            } else {
                System.out.println("FAILED TO UPLOAD!");
            }
        } catch (Exception e) {
            s3Client.abortMultipartUpload(new AbortMultipartUploadRequest(
                existingBucketName, keyName, initResponse.getUploadId()));
        }
       // return s3Client.getUrl(existingBucketName, keyName).toString();
        return  "s3://"+existingBucketName+"/"+keyName;

    }
}
