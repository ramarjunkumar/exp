package io.github.jhipster.application.service.aws;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;

import io.github.jhipster.application.UnZipFolder;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class AccessS3 {

    public AmazonS3 s3ClientConnect(String accessKey, String secretKey,
                                    String existingBucketName) {
        AWSCredentials credentials = new BasicAWSCredentials(accessKey,
            secretKey);
        AmazonS3 s3client = new AmazonS3Client(credentials);
        // s3client.setObjectAcl(existingBucketName, keyName,
        // CannedAccessControlList.Private);
        return s3client;
    }

    public void listAllObjects(String bucketName,AmazonS3 s3Client,String key) throws ParseException, IOException {


        ObjectListing objectListing = s3Client.listObjects(bucketName,key);

        System.out.println("LISTING------>"+objectListing);

        System.out.println(objectListing.getObjectSummaries());

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        for(S3ObjectSummary os : objectListing.getObjectSummaries()) {


             if(os.getKey().contains(".zip")) {

                 System.out.println("Zip file"+os.getKey());


                 System.out.println("FILES----->" + os.getKey() + "\t" +
                     sdf.format(os.getLastModified()));

                // Date date = new Date();

                 Calendar cal  = Calendar.getInstance();
                 //subtracting a day
                 cal.add(Calendar.DATE, -1);

                 String result = sdf.format(new Date(cal.getTimeInMillis()));

                // String strDate = sdf.format(date);

                 String enDate = sdf.format(os.getLastModified());


                 Date date1 = sdf.parse(result);

                 Date date2 = sdf.parse(enDate);

                 System.out.println("DATE--->"+date1+"sdfs"+date2);




                 if (date1.compareTo(date2) == 0) {

                     System.out.println("Date1 is equal to Date2");

                   downloadObject(bucketName, os.getKey(), s3Client);


                 } else {

                    // downloadObject(bucketName, "wellsfargoexp/downloads/wl_trsm_413158_201809261603.zip", s3Client);

                 }

             }

            //System.out.println(os.getKey());
        }



        //zipFolder.unzip("wl_trsm_413158_201808301603.zip");



        //zipFolder.unzip("wl_trsm_603515_201806141403_1.zip");

    }

    public void downloadObject(String bucket_name,String key_name,AmazonS3 s3Client) throws IOException {

        int index = key_name.lastIndexOf("/");
        String fileName = key_name.substring(index + 1);
        System.out.println(fileName);

                S3Object fetchFile = s3Client.getObject(new GetObjectRequest(bucket_name, key_name));
                final BufferedInputStream i = new BufferedInputStream(fetchFile.getObjectContent());
                InputStream objectData = fetchFile.getObjectContent();
               // Files.copy(objectData, new File("D:\\downloadedfile\\" + key_name).toPath()); //location to local path
                Files.copy(objectData, new File("/home/ec2-user/downloadedfile/" + fileName).toPath());
                objectData.close();

              UnZipFolder zipFolder = new UnZipFolder();
              zipFolder.unzip(fileName);
            // zipFolder.unzip(fileName);



                //zipFolder.unzip("wl_trsm_413158_201808301603.zip");


    }


    public String downloadImage(String urlName){

        String s = null;
        String errs = null;

        try {

            // run the Unix "ps -ef" command
            // using the Runtime exec method:
            String url = urlName;
            Process p = Runtime.getRuntime().exec("aws s3 presign "+url);

            BufferedReader stdInput = new BufferedReader(new
                InputStreamReader(p.getInputStream()));

            BufferedReader stdError = new BufferedReader(new
                InputStreamReader(p.getErrorStream()));

            // read the output from the command
            System.out.println("Here is the standard output of the command:\n");
            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
                return s;
            }

            // read any errors from the attempted command
            System.out.println("Here is the standard error of the command (if any):\n");
            while ((errs = stdError.readLine()) != null) {
                System.out.println(errs);
                return errs;
            }



        }
        catch (IOException e) {
            System.out.println("exception happened - here's what I know: ");
            e.printStackTrace();
            //System.exit(-1);
        }
        System.out.println("Method Response--------------------->"+s);

        return s;
    }




}
