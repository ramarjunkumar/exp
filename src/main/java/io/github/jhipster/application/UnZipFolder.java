package io.github.jhipster.application;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import io.github.jhipster.application.service.UploadToS3;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;


public class UnZipFolder  {

 public void unzip(String filename) throws IOException {

     String unzipname ="";
		 

		 try {

		      //ZipFile zipFile = new ZipFile("D:/downloadedfile/wellafargo/"+filename);
             ZipFile zipFile = new ZipFile("/home/ec2-user/downloadedfile/"+filename);
				Enumeration<?> enu = zipFile.entries();
				
				
				//File files = new File("C:\\Directory2\\Sub2\\Sub-Sub2");
				File files = new File("src/main/resources/"+"config1/");
				files.mkdir();
				while (enu.hasMoreElements()) {
					ZipEntry zipEntry = (ZipEntry) enu.nextElement();
                     
					String name = zipEntry.getName();
					long size = zipEntry.getSize();
					long compressedSize = zipEntry.getCompressedSize();
			/*		System.out.printf("name: %-20s | size: %6d | compressed size: %6d\n", 
							name, size, compressedSize);*/






                    if (filename.indexOf(".") > 0) {

                        unzipname = filename.substring(0, filename.lastIndexOf("."));

                    }





                    File file = new File(files+"/"+unzipname+"/"+name);
					if (name.endsWith("/")) {
						file.mkdirs();
						continue;
					}

					File parent = file.getParentFile();
					if (parent != null) {
						parent.mkdirs();
					}

					InputStream is = zipFile.getInputStream(zipEntry);
					 FileOutputStream fos = new FileOutputStream(file);
					byte[] bytes = new byte[1024];
					int length;
					while ((length = is.read(bytes)) >= 0) {
						fos.write(bytes, 0, length);
					}
					is.close();
					fos.close();



                  }
				zipFile.close();



             System.out.println("Before thread");


             /*UnZipFolder c = new UnZipFolder();
             c.start();*/


                 String location="src/main/resources/config1/"+unzipname;
                 FinalTif2Pdf hd=new FinalTif2Pdf();
                 hd.tiffPdf(location);
                 System.out.println("~~~~~~~~~~~~~~~~~~~~ "+hd.getFileLocation());
                 //Thread.sleep(10000);

             FinalPdf2Jpg dh=new FinalPdf2Jpg();
             dh.pdfjpg(location);
             System.out.println("~~~~~~~~~~~~~~~~~~~~ "+dh.getJpgfileLocation());



                 List<String> pdffiles = new ArrayList<String>();
                 pdffiles=hd.getFileLocation();
                 System.out.println("pdffiles................%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5.."+pdffiles);

                 List<String> jpgfiles = new ArrayList<String>();
                 jpgfiles=dh.getJpgfileLocation();
                 System.out.println("jpgfiles........%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%..."+jpgfiles);



             if(pdffiles!=null){

                 ReadTextFile textfile = new ReadTextFile();
                 textfile.getFile("fields.dat","images.dat",unzipname,pdffiles,jpgfiles);

             }




             }


					
             catch (IOException e) {
				e.printStackTrace();
			}

		 
		}

}
