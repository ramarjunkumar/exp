package io.github.jhipster.application.web.rest;

import java.net.URISyntaxException;
import java.util.*;

import io.github.jhipster.application.service.aws.AccessS3;
import org.springframework.beans.factory.annotation.Autowired;
import io.github.jhipster.web.util.ResponseUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.application.domain.Fields;
import io.github.jhipster.application.domain.FieldsBean;
import io.github.jhipster.application.domain.ImagesBean;
import io.github.jhipster.application.repository.FieldsRepository;
import io.github.jhipster.application.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping("/api")
public class FieldsResource {

    private static final String ENTITY_NAME = "Fields";

	@Autowired
	private static FieldsRepository fieldsRepository;

	public FieldsResource(FieldsRepository fieldsRepository) {
		this.fieldsRepository = fieldsRepository;
	}

	public final static void insertData(List<FieldsBean> fieldlist, List<ImagesBean> imageList,List<String> pdflist,List<String>jpeglist)

	{

        System.out.println("Field Size---->"+fieldlist.size());
        System.out.println("PDF Size---->"+pdflist.size()+"list"+pdflist);
        System.out.println("JPEG Size---->"+jpeglist.size()+"LIST"+jpeglist);


        if(fieldlist.size() != 0){


            for (int i = 0; i < fieldlist.size(); i++) {

                FieldsBean field = fieldlist.get(i);
                ImagesBean images = imageList.get(i);

                String pdf = pdflist.get(i);

                String jpeg = jpeglist.get(i);

                System.out.println(field.getCheckDate());
                System.out.println(field.getInvoiceNo());
                System.out.println(images.getAccount_number());
                System.out.println(images.getAmount());
                System.out.println(images.getRouting_number());


                Fields fieldentity = new Fields();
                fieldentity.setAccountnumber(images.getAccount_number());
                fieldentity.setAmount(images.getAmount());
                fieldentity.setCheck_date(field.getCheckDate());
                fieldentity.setInvoiceNumber(field.getInvoiceNo());
                fieldentity.setRoutingNumber(images.getRouting_number());
                fieldentity.setPdfPath(pdf);
                fieldentity.setJpegPath(jpeg);

                fieldsRepository.save(fieldentity);

                // System.out.println(pdfpath);
                // System.out.println(jpegpaath);

            }


        }

        else{


            for (int i = 0; i < imageList.size(); i++) {


                ImagesBean images = imageList.get(i);


                String pdf = pdflist.get(i);

                String jpeg = jpeglist.get(i);

                Fields fieldentity = new Fields();
                fieldentity.setAccountnumber(images.getAccount_number());
                fieldentity.setAmount(images.getAmount());
                fieldentity.setCheck_date(null);
                fieldentity.setInvoiceNumber(null);
                fieldentity.setRoutingNumber(images.getRouting_number());

                fieldentity.setPdfPath(pdf);
                fieldentity.setJpegPath(jpeg);

                fieldsRepository.save(fieldentity);

                // System.out.println(pdfpath);
                // System.out.println(jpegpaath);

            }

        }


	}

	@GetMapping("/field-all")
	@Timed
	public ResponseEntity<List<Fields>> fieldList(@ApiParam Pageable pageable) {
		// List<Fields> listName=new ArrayList<Fields>();
		Page<Fields> listField = fieldsRepository.findByDeletedIsFalseAndInvoiceNumberNotNull(pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(listField, "/api/field-all");
		return new ResponseEntity<>(listField.getContent(), headers, HttpStatus.OK);
	}

	/**
     * GET /field/search : get field by invoice number.
     *
     * @param invoiceNumber the invoice number
     * @return the ResponseEntity with status 200 (OK) and with body invoice number
     */
	@GetMapping("/field/search")
	@Timed
	public ResponseEntity<List<Fields>> searchOpportunities(@RequestParam(value = "invoiceNumber") String invoiceNumber)
			throws URISyntaxException {

		List<Fields> result = fieldsRepository.findByInvoiceNumber(invoiceNumber);

		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(result));

	}


	@GetMapping("/field/delete")
	@Timed
	public ResponseEntity<List<Fields>> deleteTransaction(@RequestParam(value = "invoiceNumber") String invoiceNumber)
			throws URISyntaxException {

		List<Fields> result = fieldsRepository.findByInvoiceNumber(invoiceNumber);
		result.get(0).setDeleted(true);
		fieldsRepository.save(result.get(0));
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(result));

	}

	@GetMapping("/field-all/page")
	@Timed
	public ResponseEntity<List<Fields>> getAllField(@ApiParam Pageable pageable) {
		// List<Fields> listName=new ArrayList<Fields>();
		Page<Fields> listField = fieldsRepository.findAll(pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(listField, "/api/field-all/page");
		return new ResponseEntity<>(listField.getContent(), headers, HttpStatus.OK);
	}

    @GetMapping("/field-all/withoutid")
    @Timed
    public ResponseEntity<List<Fields>> getAllFieldWithoutTransId(@ApiParam Pageable pageable) {
        Page<Fields> listField = fieldsRepository.findByInvoiceNumberIsNull(pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(listField, "/api/field-all/withoutid");
        return new ResponseEntity<>(listField.getContent(), headers, HttpStatus.OK);
    }


    @PutMapping("/field/transid")
    @Timed
    public ResponseEntity<Fields> saveFieldsWithId(@RequestBody Fields field) throws URISyntaxException {
        // List<Fields> listName=new ArrayList<Fields>();

         Fields result  =  fieldsRepository.save(field);

        return ResponseEntity.ok(result);


	}


    @PostMapping("/field/findid")
    @Timed
    public ResponseEntity<Optional<Fields>> getFields(@RequestBody Fields field) throws URISyntaxException {
        // List<Fields> listName=new ArrayList<Fields>();

       Optional<Fields> result =  fieldsRepository.findById(field.getId());

        return ResponseEntity.ok(result);


    }

    @GetMapping("/field/download")
    @Timed
    public ResponseEntity<String> download(@RequestParam(value = "urlvalue") String urlvalue) {

        System.out.println("URL>>>>>>>>>>>>"+urlvalue);


	    String url = urlvalue;

        AccessS3 accessS3 = new AccessS3();

       String retVal = accessS3.downloadImage(url);

        System.out.println("REturned---------->"+retVal);

       return new ResponseEntity<>("{\"success\": true, \"url\": \"" + retVal + "\"}",HttpStatus.CREATED);




    }



}

