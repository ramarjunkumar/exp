package io.github.jhipster.application.domain;

import java.io.Serializable;

public class ImagesBean implements Serializable {
	
	
private String amount;
	
    private String routing_number;
    
    private String account_number;
    
    private String pdfpath;
    
    private String jpegpath;

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getRouting_number() {
		return routing_number;
	}

	public void setRouting_number(String routing_number) {
		this.routing_number = routing_number;
	}

	public String getAccount_number() {
		return account_number;
	}

	public void setAccount_number(String account_number) {
		this.account_number = account_number;
	}
	
	

	public String getPdfpath() {
		return pdfpath;
	}

	public void setPdfpath(String pdfpath) {
		this.pdfpath = pdfpath;
	}

	public String getJpegpath() {
		return jpegpath;
	}

	public void setJpegpath(String jpegpath) {
		this.jpegpath = jpegpath;
	}

	@Override
	public String toString() {
		return "ImagesBean [amount=" + amount + ", routing_number=" + routing_number + ", account_number="
				+ account_number + ", pdfpath=" + pdfpath + ", jpegpath=" + jpegpath + "]";
	}





}
