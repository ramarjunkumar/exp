package io.github.jhipster.application.domain;

public class FieldsBean {
	
	private String checkDate;
	private String invoiceamt;
	private String invoiceNo;
	
	public String getCheckDate() {
		return checkDate;
	}
	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}
	public String getInvoiceamt() {
		return invoiceamt;
	}
	public void setInvoiceamt(String invoiceamt) {
		this.invoiceamt = invoiceamt;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	@Override
	public String toString() {
		return "FieldsBean [checkDate=" + checkDate + ", invoiceamt=" + invoiceamt + ", invoiceNo=" + invoiceNo + "]";
	}

}
