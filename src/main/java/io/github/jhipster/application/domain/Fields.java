package io.github.jhipster.application.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name="fields")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Fields implements Serializable {
	
	
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
	private long id;
	 
	 @Column(name="InvoiceNumber")
	private String invoiceNumber;
	 @Column(name="CheckDate")
	private String check_date;
	 @Column(name="accountNumber")
	 private String accountnumber;
	 @Column(name="amount")
	 private String amount;
	 @Column(name="routingNumber")
	 private String routingNumber;
	 
	 @Column(name="pdfPath")
	 private String pdfPath;
	 
	 @Column(name="jpegPath")
	 private String jpegPath;

	 @Column(name="deleted")
	 private boolean deleted;

	
	 
	 

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}



	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getCheck_date() {
		return check_date;
	}

	public void setCheck_date(String check_date) {
		this.check_date = check_date;
	}

	public String getAccountnumber() {
		return accountnumber;
	}

	public void setAccountnumber(String accountnumber) {
		this.accountnumber = accountnumber;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getRoutingNumber() {
		return routingNumber;
	}

	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}

	public String getPdfPath() {
		return pdfPath;
	}

	public void setPdfPath(String pdfPath) {
		this.pdfPath = pdfPath;
	}

	public String getJpegPath() {
		return jpegPath;
	}

	public void setJpegPath(String jpegPath) {
		this.jpegPath = jpegPath;
	}

 
	public boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	

}
