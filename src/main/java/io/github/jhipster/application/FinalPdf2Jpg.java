package io.github.jhipster.application;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import io.github.jhipster.application.service.UploadToS3;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;

public class FinalPdf2Jpg {

    String jpg_dh="";

    public List<String> JpgfileLocation=new ArrayList<String>();;

    public List<String> getJpgfileLocation() {
        return JpgfileLocation;
    }

    public void setJpgfileLocation(List<String> jpgfileLocation) {
        JpgfileLocation = jpgfileLocation;
    }

    File outputfile;

    public void pdfjpg(String sFilesDir) {
        File[] filesInDirectory = new File(sFilesDir).listFiles();
        String finalPath = sFilesDir;
        for(File f : filesInDirectory){
            if(f.isDirectory()){
                pdfjpg(f.getAbsolutePath());
                finalPath=f.getAbsolutePath();
            }
            else {
                processjpeg(finalPath);
                break;
            }
        }

    }

    public void processjpeg(String sFilesDir) {

        File[] filesInDirectory = new File(sFilesDir).listFiles();
        for(File f : filesInDirectory){
            if(f.exists()) {
                String filePath = f.getAbsolutePath();
               // System.out.println("filepath jpg............................"+filePath);
                String fileExtension = filePath.substring(filePath.lastIndexOf(".") + 1,filePath.length());
                //System.out.println("fileExtension.........................."+fileExtension);

                try {
                    if("pdf".equals(fileExtension)){
                        String destinationDir = sFilesDir;
                        File sourceFile = new File(filePath);
                     //   System.out.println("sourceFile.........."+sourceFile);
                        String reqpath=sourceFile.getPath();
                      //  System.out.println("reqpath............"+reqpath);
                        String destinationFile = sourceFile.getName().replace(".pdf", "");
                        String subscr = "src";
                        String[] partscr = reqpath.split(subscr);
                        String part1 = partscr[0];
                      //  System.out.println("part1....with slash..............."+part1);
                     //   System.out.println("destinationFile............."+destinationFile);


                        if (sourceFile.exists()) {
                            PDDocument document = PDDocument.load(filePath);
                            List<PDPage> list = document.getDocumentCatalog().getAllPages();
                            int pageNumber = 1;
                            for (PDPage page : list) {
                                if(pageNumber==1) {
                                    BufferedImage image = page.convertToImage();
                                    outputfile = new File(part1+destinationDir, destinationFile+".jpg");
                                  //  System.out.println("outputfile..........."+outputfile);
                                    ImageIO.write(image, "jpg", outputfile);
                                    pageNumber++;
                                }
                            }
                            document.close();
                          //  System.out.println("PATH>>>>>>>>>>>>>>>>>>>>>>>"+sFilesDir);

                            UploadToS3 awsConnect = new UploadToS3();
                            String bucketName = "";
                            String keyName = "";
                            String str=outputfile.getAbsolutePath();
                          //  System.out.println("str............................"+str);
                            String substr = "config";
                            String[] parts = str.split(substr);
                           // System.out.println("parts..........................."+parts);
                            String after = parts[1];
                           // System.out.println("after..........................."+after);
                            //String after1 = after.split("" + f.getName())[0];
                            String Finalsplitpath =after.substring(0, after.length());
                           // System.out.println("........>>>>>>>>pdf........."+Finalsplitpath);
                            Finalsplitpath=Finalsplitpath.replace("/", "");
                            //System.out.println("FINAL>>>>>>>>pdf........."+Finalsplitpath);

                            AmazonS3 s3Client = new AmazonS3Client(DefaultAWSCredentialsProviderChain.getInstance());
                            UploadToS3 s3 = new UploadToS3();
				            jpg_dh=s3.S3Upload(s3Client, "exp-sandbox-wellsfargo-output/", Finalsplitpath,
                                str);
                            JpgfileLocation.add(jpg_dh);
                            //System.out.println(JpgfileLocation);
                        }
                        else {
                            System.err.println(sourceFile.getName() +" File not exists");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

    }

}
