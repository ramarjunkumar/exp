package io.github.jhipster.application.repository;

import io.github.jhipster.application.domain.Fields;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@SuppressWarnings("unused")
@Repository

public interface FieldsRepository extends JpaRepository<Fields, Long> {
	
	List<Fields> findByInvoiceNumber(String invoiceNumber);
	Page<Fields> findByDeletedIsFalseAndInvoiceNumberNotNull(Pageable pageable);

    /* @Query(
        value = "SELECT * FROM fields  WHERE invoice_number is null ORDER BY ?#{#pageable}",
        nativeQuery = true)     
      Page<Fields> findAllWithoutTransID(Pageable pageable); */

      Page<Fields> findByInvoiceNumberIsNull(Pageable pageable);

       Optional<Fields> findById(Long id);

}
