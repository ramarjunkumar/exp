import { Route } from '@angular/router';
import { UserRouteAccessService } from 'app/core';

import { DashboardComponent } from './';

export const DASHBOARD_ROUTE: Route = {
    path: 'dashboard',
    component: DashboardComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'Dashboard'
    },
    canActivate: [UserRouteAccessService]
};
