import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Rx';
import { Dashboard } from './dashboard.model';
import { createRequestOption } from 'app/shared';
import { SERVER_API_URL } from 'app/app.constants';
declare var $: any;

@Injectable()
export class DashboardService {
    constructor(private http: HttpClient) {}

    list(req?: any): Observable<HttpResponse<Dashboard[]>> {
        const options = createRequestOption(req);
        return this.http.get<Dashboard[]>(SERVER_API_URL + 'api/field-all', { params: options, observe: 'response' });
    }

    nulllists(req?: any): Observable<HttpResponse<Dashboard[]>> {
        const options = createRequestOption(req);
        return this.http.get<Dashboard[]>(SERVER_API_URL + 'api/field-all/withoutid', { params: options, observe: 'response' });
    }

    search(query: string): Observable<any> {
        return this.http.get(SERVER_API_URL + 'api/field/search?invoiceNumber=' + query);
    }

    delete(inVoice_number: string) {
        return this.http.get(SERVER_API_URL + 'api/field/delete' + '/?invoiceNumber=' + inVoice_number);
    }

    download(pdf_path: string) {
        return this.http.get(SERVER_API_URL + 'api/field/download' + '/?urlvalue=' + pdf_path);
    }
}
