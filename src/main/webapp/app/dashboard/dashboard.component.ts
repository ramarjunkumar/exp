import { Component, OnInit } from '@angular/core';
import { DashboardService } from './dashboard.service';
import { Dashboard } from './dashboard.model';
import { SweetAlertService } from 'ngx-sweetalert2';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiAddDialogComponent } from 'app/shared';
import { NgxPaginationModule } from 'ngx-pagination';
import { JhiParseLinks } from 'ng-jhipster';
import { HttpResponse } from '@angular/common/http';
import { Location } from '@angular/common';

@Component({
    selector: 'jhi-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['dashboard.css'],
    providers: [SweetAlertService]
})
export class DashboardComponent implements OnInit {
    dashboards: Dashboard[] = [];
    nullDashboards: Dashboard[] = [];
    transactionId: string;
    searchQuery: string;
    inVoice_number: string;
    pdf_path: string;
    links: any;
    listPage: number;
    listItesmsPerPage: number;
    listTotalItems: number;
    nullListPage: number;
    nullListItesmsPerPage: number;
    nullListTotalItems: number;
    pageNumber: number;
    jpeg_url: string;

    constructor(
        private dashboardService: DashboardService,
        private swal2: SweetAlertService,
        private dialog: NgbModal,
        private parseLinks: JhiParseLinks,
        private _location: Location
    ) {}

    ngOnInit() {
        this.nullListPage = 0;
        this.nullListItesmsPerPage = 10;
        this.listPage = 0;
        this.listItesmsPerPage = 10;
        this.getLists(this.pageNumber);
        this.getNullLists(this.pageNumber);
    }

    backClicked() {
        this._location.back();
    }

    getLists(pageNumber?: number) {
        if (pageNumber) {
            this.listPage = pageNumber;
        }
        this.dashboards = [];
        this.dashboardService
            .list({
                page: this.listPage - 1,
                size: this.listItesmsPerPage
            })
            .subscribe(
                (res: HttpResponse<Dashboard[]>) => this.success1(res.body, res.headers),
                (res: HttpResponse<any>) => this.error1(res.body)
            );
    }

    getNullLists(pageNumber?: number) {
        if (pageNumber) {
            this.nullListPage = pageNumber;
        }
        this.nullDashboards = [];
        this.dashboardService
            .nulllists({
                page: this.nullListPage - 1,
                size: this.nullListItesmsPerPage
            })
            .subscribe(
                (res: HttpResponse<Dashboard[]>) => this.success(res.body, res.headers),
                (res: HttpResponse<any>) => this.error(res.body)
            );
    }

    success(data, headers) {
        console.log(data);
        console.log(headers);
        this.links = this.parseLinks.parse(headers.get('link'));
        this.nullListTotalItems = headers.get('x-total-count');
        this.nullDashboards = data;
        console.log(this.nullDashboards);
    }

    error(data) {
        console.log(data);
    }

    success1(data, headers) {
        console.log(data);
        console.log(headers);
        this.links = this.parseLinks.parse(headers.get('link'));
        this.listTotalItems = headers.get('x-total-count');
        this.dashboards = data;
        console.log(this.dashboards);
    }

    error1(data) {
        console.log(data);
    }

    onSubmit() {
        this.dashboards = [];
        if (this.searchQuery === null || this.searchQuery === '') {
            this.getLists(this.pageNumber);
        } else {
            this.dashboardService.search(this.searchQuery).subscribe(transactionId => {
                this.dashboards = transactionId;
            });
        }
    }

    allLists(event) {
        this.getLists();
    }

    deleteRecord(dashboard): void {
        this.swal2
            .success({
                title: 'Are you sure?',
                text: 'Record will be removed',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            })
            .then(result => {
                if (result) {
                    this.dashboardService.delete(dashboard.invoiceNumber).subscribe(data => {
                        this.dashboards = this.dashboards.filter(u => u !== dashboard);
                    });
                }
            });
    }

    openAddDialog(id?: number, transaction?: any) {
        const modalRef = this.dialog.open(JhiAddDialogComponent);
        modalRef.componentInstance.transaction = transaction;
        modalRef.componentInstance.onFormSubmit.subscribe(action => {
            if (action === 'submitted') {
                console.log('submitted');
                this.getLists();
                this.getNullLists();
            } else {
                console.log('cancelled');
            }
        });
    }
    download(pdfPath) {
        this.dashboardService.download(pdfPath).subscribe(
            data => {
                console.log('url', data['url']);
                const path = data['url'];
                window.open(path);
                this.pdf_path = pdfPath;
                console.log('testing', this.pdf_path);
                console.log('testing', pdfPath);
            },
            error => {
                console.log('err...', error);
            }
        );
    }
}
