import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';

import { ExpSharedModule } from 'app/shared';
import { DASHBOARD_ROUTE, DashboardComponent } from './';
import { DashboardService } from './dashboard.service';

@NgModule({
    imports: [ExpSharedModule, NgxPaginationModule, RouterModule.forChild([DASHBOARD_ROUTE])],
    declarations: [DashboardComponent],
    providers: [DashboardService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExpDashboardModule {}
