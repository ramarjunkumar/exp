import { Component, OnInit } from '@angular/core';

import { LoginService, Principal, Account } from 'app/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: ['home.css']
})
export class HomeComponent implements OnInit {
    account: Account;

    constructor(private principal: Principal, private loginService: LoginService, private router: Router) {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                console.log('home navigation...');
            }
        });
    }

    ngOnInit() {
        console.log('on init home...');
        this.principal.identity().then(account => {
            console.log('account exists...');
            this.account = account;
            if (account) {
                this.router.navigate(['/dashboard']);
            }
        });
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.loginService.login();
    }
}
