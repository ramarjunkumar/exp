export class Dashboard {
    constructor(
        public id: Number,
        public amount: Number,
        public accountnumber: Number,
        public routingNumber: Number,
        public check_date: Number,
        public inVoice_number: string
    ) {}
}
