import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Rx';
import { Dashboard } from '../dashboard/dashboard.model';
import { createRequestOption } from 'app/shared';
import { SERVER_API_URL } from 'app/app.constants';

declare var $: any;

@Injectable()
export class EditService {
    constructor(private http: HttpClient) {}

    update(id: string, dashboard: Dashboard) {
        console.log(id);
        return this.http.put(SERVER_API_URL + 'api/field/transid' + '/?id=' + id, dashboard);
    }
}
