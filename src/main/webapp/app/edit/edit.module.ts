import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ExpSharedModule } from 'app/shared';
import { EDIT_ROUTE, EditComponent } from './';
import { EditService } from './edit.service';

@NgModule({
    imports: [ExpSharedModule, RouterModule.forChild([EDIT_ROUTE])],
    declarations: [EditComponent],
    providers: [EditService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExpEditModule {}
