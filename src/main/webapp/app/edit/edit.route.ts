import { Route } from '@angular/router';
import { UserRouteAccessService } from 'app/core';

import { EditComponent } from './edit.component';

export const EDIT_ROUTE: Route = {
    path: 'edit/:id',
    component: EditComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'Edit'
    },
    canActivate: [UserRouteAccessService]
};
