import { Injectable } from '@angular/core';

import { Principal } from '../auth/principal.service';
import { AuthServerProvider } from '../auth/auth-session.service';
import { JhiTrackerService } from '../tracker/tracker.service';
import { Router } from '@angular/router';
import { SERVER_API_URL } from 'app/app.constants';

@Injectable({ providedIn: 'root' })
export class LoginService {
    constructor(private principal: Principal, private trackerService: JhiTrackerService, private authServerProvider: AuthServerProvider, private router: Router) {}

    login() {
        let port = location.port ? ':' + location.port : '';
        if (port === ':9000') {
            port = ':8080';
        }
        // location.href = '//' + location.hostname + port + '/login';
        location.href = SERVER_API_URL + '/login';
    }

    logout() {
        this.authServerProvider.logout().subscribe();
        this.principal.authenticate(null);
    }
}
