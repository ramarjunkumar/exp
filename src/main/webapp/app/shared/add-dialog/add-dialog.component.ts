import { Component, AfterViewInit, Renderer, ElementRef, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { JhiEventManager } from 'ng-jhipster';
import { SweetAlertService } from 'ngx-sweetalert2';

import { EditService } from 'app/edit/edit.service';

@Component({
    selector: 'jhi-add-dialog',
    templateUrl: './add-dialog.component.html',
    providers: [SweetAlertService]
})
export class JhiAddDialogComponent implements AfterViewInit {
    invoiceNumber: number;
    transaction: any;
    formSubmitted: boolean;
    onFormSubmit = new EventEmitter();

    constructor(
        private eventManager: JhiEventManager,
        private elementRef: ElementRef,
        private renderer: Renderer,
        private router: Router,
        public activeModal: NgbActiveModal,
        public editService: EditService,
        private swal2: SweetAlertService
    ) {
        this.formSubmitted = false;
    }

    ngAfterViewInit() {
        this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#invoice_number'), 'focus', []);
    }

    onSubmit() {
        this.transaction['invoiceNumber'] = this.invoiceNumber;
        if (this.transaction.id) {
            this.editService.update(this.transaction.id, this.transaction).subscribe(() => {
                this.formSubmitted = true;
                this.onFormSubmit.emit('submitted');
                this.activeModal.close();
                this.swal2.success({
                    title: 'Successfully Added',
                    animation: false,
                    customClass: 'animated tada'
                });
            });
        } else {
            this.formSubmitted = false;
            this.swal2.success({
                title: 'Please enter Transaction ID'
            });
        }
    }

    cancel() {
        this.formSubmitted = false;
        this.activeModal.close();
    }
}
