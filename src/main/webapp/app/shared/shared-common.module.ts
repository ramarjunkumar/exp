import { NgModule } from '@angular/core';

import { ExpSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [ExpSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [ExpSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class ExpSharedCommonModule {}
