import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

import { NgbDateMomentAdapter } from './util/datepicker-adapter';
import { ExpSharedLibsModule, ExpSharedCommonModule, HasAnyAuthorityDirective } from './';
import { JhiAddDialogComponent } from './add-dialog/add-dialog.component';

@NgModule({
    imports: [ExpSharedLibsModule, ExpSharedCommonModule],
    declarations: [HasAnyAuthorityDirective, JhiAddDialogComponent],
    providers: [{ provide: NgbDateAdapter, useClass: NgbDateMomentAdapter }],
    exports: [ExpSharedCommonModule, HasAnyAuthorityDirective],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExpSharedModule {}
